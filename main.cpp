#include <iostream>

#include <list>
#include <algorithm>
#include <thread>
#include <mutex>
#include <signal.h>

static volatile sig_atomic_t profFlag = 0;
static volatile sig_atomic_t interruptFlag = 0;

static std::mutex mutex;

static const int VALUES_COUNT = 100;

class TaskThread;
std::list<TaskThread *> processThreads;

void task()
{
    int values[VALUES_COUNT];

    for (int i = 0; i < VALUES_COUNT; ++i)
    {
        values[i] = i;
    }

    /// simulation of solving NP-complete task
    while (true)
    {
        {
            std::lock_guard<std::mutex> guard(mutex);
            if (interruptFlag)
            {
                return;
            }

            if (profFlag)
            {
                for (TaskThread *thread : processThreads)
                {
                    thread->profile();
                }
                profFlag = 0;
            }
            mutex.unlock();
        }

        std::next_permutation(values, values + VALUES_COUNT);

        static const std::chrono::nanoseconds sleepInterval(1);
        std::this_thread::sleep_for(sleepInterval);
    }
}

/// ---------------------------------------------------------------- ///

class TaskThread
{
public:
    TaskThread();
    virtual ~TaskThread();

    void profile();
    bool checkAndPopProfile();

    virtual void run() = 0;

protected:
    std::mutex mMutex;
    bool mProfileFlag;
    std::thread *mThread;
};

TaskThread::TaskThread()
    : mProfileFlag(false),
      mThread(nullptr)
{ }

TaskThread::~TaskThread()
{
    if (mThread)
    {
        mThread->join();
        delete mThread;
    }
}

void TaskThread::profile()
{
    std::lock_guard<std::mutex> lock(mMutex);
    mProfileFlag = true;
}

bool TaskThread::checkAndPopProfile()
{
    std::lock_guard<std::mutex> lock(mMutex);
    bool res = mProfileFlag;
    mProfileFlag = false;
    return res;
}

/// ---------------------------------------------------------------- ///

class MainThread : public TaskThread
{
public:
    void run()
    {
        task();
    }
};

class WorkerThread : public TaskThread
{
public:
    void run()
    {
        mThread = new std::thread(task);
    }
};

/// ---------------------------------------------------------------- ///

/** Signals will be delivered to random thread of process */
static void signalHandler(int sig, siginfo_t *siginfo, void *context)
{
    std::cout << "Thread " << std::this_thread::get_id() << " received signal " << sig << std::endl;

    std::lock_guard<std::mutex> guard(mutex);
    if (sig == SIGPROF)
    {
        profFlag = 1;
    }
    else if (sig == SIGINT)
    {
        interruptFlag = 1;
    }
}

int main()
{
    std::cout << "Hello World!" << std::endl;
    std::cout << "Main Thread " << std::this_thread::get_id() << std::endl;

    struct sigaction action;
    action.sa_sigaction = signalHandler;

    sigaction(SIGINT, &action, NULL);

    TaskThread *workerThread = new WorkerThread;
    TaskThread *mainThread = new MainThread;

    processThreads.push_back(mainThread);
    processThreads.push_back(workerThread);

    workerThread->run();
    mainThread->run();

    delete workerThread;
    delete mainThread;

    return 0;
}

