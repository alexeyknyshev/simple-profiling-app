# Simple self-profiling application #

Simple self-profiling application in C++ using Unix signal handling mechanism

* Profiling is optional – the application is able to be run with and without profiling without
* When interrupted by the user (Control-C), the workloads in both threads are stopped gracefully,and the application wait until the second thread finishes before exiting.
* Application can receive SIGPROF signal regularly.
* In SIGPROF signal handler app collects the following information:
> * ID of the current thread.
> * ID of the CPU on which the current thread is being executed.
> * Timestamp.
> * Instruction pointer (or program counter) – where the application has been interrupted.
* Collected data is serialized & saved into a file.